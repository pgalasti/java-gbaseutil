/*
 * (C) Copyright 2015 Paul Galasti
 *
 * https://bitbucket.org/pgalasti
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */
package com.galasti.gbaseutil.string;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class LineParserTest {

	@BeforeClass
	public static void setup() {
		System.out.println("=======================================");
		System.out.println("Performing LineParserTest Unit Tests...");
		System.out.println("=======================================");
	}
	
	@AfterClass
	public static void breakdown() {
		System.out.println("==================================");
		System.out.println("LineParserTest Unit Tests Complete");
		System.out.println("==================================");
	}
	
	public static final String TEST_LINE1 =
			"This is a line to confirm the parser is functioning "
			+ "correctly"; 
	
	public static final String TEST_LINE2 = 
			"This-is-another-test-line-with-a-different-delimiter";
	
	@Test
	public void testParsing() {
		System.out.println("Testing String token parsing...");
		
		LineParser parser = new LineParser();
		
		List<String> tokens;
		
		// Parse the test line with space delimiter
		parser.setLine(TEST_LINE1);
		parser.setDelimiter(' ');
		parser.parse();
		tokens = parser.getTokenList();
		
		// Test out some of the parsed tokens
		assertTrue(tokens.get(3).compareTo("line") == 0);
		assertTrue(tokens.get(7).compareTo("parser") == 0);
		assertTrue(tokens.get(10).compareTo("correctly") == 0);
		
		//Parse another test line with a different delimiter
		parser.setLine(TEST_LINE2);
		parser.setDelimiter('-');
		parser.parse();
		tokens = parser.getTokenList();

		// Test out some of the parsed tokens
		assertTrue(tokens.get(3).compareTo("test") == 0);
		assertTrue(tokens.get(7).compareTo("different") == 0);
		assertTrue(tokens.get(8).compareTo("delimiter") == 0);
		assertTrue(tokens.get(0).compareTo("This") == 0);
		
		System.out.println("Complete\n");
	}
}

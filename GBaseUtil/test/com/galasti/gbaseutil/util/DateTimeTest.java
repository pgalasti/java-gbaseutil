/*
 * (C) Copyright 2015 Paul Galasti
 *
 * https://bitbucket.org/pgalasti
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */
package com.galasti.gbaseutil.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class DateTimeTest {

	@BeforeClass
	public static void setup() {
		System.out.println("=====================================");
		System.out.println("Performing DateTimeTest Unit Tests...");
		System.out.println("=====================================");
	}
	
	@AfterClass
	public static void breakdown() {
		System.out.println("================================");
		System.out.println("DateTimeTest Unit Tests Complete");
		System.out.println("================================");
	}
	
	@Test
	public void testDifferenceBetweenDates() {
		System.out.println("Testing time between dates...");
		
		DateTime date1; 
		DateTime date2; 
		
		date1 = new DateTime(5, 17, 1987);
		date2 = new DateTime(8, 17, 1987);
		final int days1 = DateTime.differenceBetweenDates(date1, date2);
		assertEquals(92, days1);
		
		date1 = new DateTime(5, 17, 1987);
		date2 = new DateTime(5, 17, 2015);
		final int days2 = DateTime.differenceBetweenDates(date1, date2);
		assertEquals(10227, days2);
		
		date1 = new DateTime(5, 17, 1987);
		date2 = new DateTime(8, 17, 1987);
		final int days3 = DateTime.differenceBetweenDates(date2, date1);
		assertEquals(-92, days3);
		
		date1 = new DateTime(5, 17, 1987);
		date2 = new DateTime(5, 17, 2015);
		final int days4 = DateTime.differenceBetweenDates(date2, date1);
		assertEquals(-10227, days4);
		
		System.out.println("Complete\n");
	}
	
	@Test
	public void testTimeSetting() {
		System.out.println("Testing date/time settings...");
		
		DateTime date;
		
		// 548222400000
		date = new DateTime(5,17,1987);
		assertTrue(date.getTimeInMilliseconds() == 548222400000L);
		
		// 548222400000
		date = new DateTime(5,17,1987,0 ,0 ,0 ,0);
		assertTrue(date.getTimeInMilliseconds() == 548222400000L);
		
		// 1431835200000
		date = new DateTime(5,17,2015);
		assertTrue(date.getTimeInMilliseconds() == 1431835200000L);
		
		// 1431835200000
		date = new DateTime(5,17,2015,0 ,0 ,0 ,0);
		assertTrue(date.getTimeInMilliseconds() == 1431835200000L);

		// 05/17/1987
		date = new DateTime(548222400000L);
		assertTrue(date.getMonth() == 5);
		assertTrue(date.getDayOfMonth() == 17);
		assertTrue(date.getYear() == 1987);
		
		// 05/17/2015
		date = new DateTime(1431835200000L);
		assertTrue(date.getMonth() == 5);
		assertTrue(date.getDayOfMonth() == 17);
		assertTrue(date.getYear() == 2015);
		
		// 548222400000+10800000 = 548233200000
		date = new DateTime(5, 17, 1987, 3, 0, 0, 0);
		assertTrue(date.getTimeInMilliseconds() == 548233200000L);
		date.setTimeToStartOfDay();
		assertTrue(date.getTimeInMilliseconds() == 548222400000L);
		
		System.out.println("Complete\n");
	}
	
	@Test
	public void testDateStringFormats() {
		System.out.println("Testing date/time string formats...");
		
		
		
		System.out.println("Complete\n");
	}

}

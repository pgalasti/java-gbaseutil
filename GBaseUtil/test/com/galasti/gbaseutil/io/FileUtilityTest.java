/*
 * (C) Copyright 2015 Paul Galasti
 *
 * https://bitbucket.org/pgalasti
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */
package com.galasti.gbaseutil.io;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Paul Galasti
 */
public class FileUtilityTest {
    
    @BeforeClass
    public static void setUpClass() {
        
        System.out.println("====================================\n");
        System.out.println("File Utility test starting...");
        System.out.println("====================================\n");
        
        
    }
    
    @AfterClass
    public static void tearDownClass() {
        
        
        
        System.out.println("====================================\n");
        System.out.println("File Utility test complete!");
        System.out.println("====================================\n");
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testUserHomeDirectory() {
        
        System.out.println("Testing user home directory operations...");
        
        final String userHomeDirectoryPath = FileUtility.getUserHomeDirectory();
        
        final File userHomeDirectory = new File(userHomeDirectoryPath);
        assertTrue(userHomeDirectory.canRead());
        assertTrue(userHomeDirectory.canWrite());
        
        final String uuid = UUID.randomUUID().toString();
        final String tempFile = userHomeDirectory.getAbsolutePath() 
                + File.separatorChar + uuid + "File_Utility_Unit_Test.txt";
        try {
            
            assertTrue(new File(tempFile).createNewFile());
            
        } catch(IOException e) {
            throw new RuntimeException("Unable to create temp file during "
                    + "unit test!", e);
        } finally {
            
            if(new File(tempFile).exists()) {
                new File(tempFile).delete();
            }
        }
        
        System.out.println("User home directory operation testing complete.\n");
    }
    
    @Test
    public void testCopyOperations() {
        
        System.out.println("Testing copy operations...");

        final String uniqueFileSourceName = UUID.randomUUID().toString() + ".txt";
        final String uniqueFileDestName = UUID.randomUUID().toString() + ".txt";
        
        String tempFileSource = FileUtility.getUserHomeDirectory() 
                + File.separatorChar + uniqueFileSourceName;
        String tempFileDest = FileUtility.getUserHomeDirectory() 
                + File.separatorChar + uniqueFileDestName;
        
        try {
            assertTrue(new File(tempFileSource).createNewFile());
                    
            FileUtility.copy(tempFileSource, tempFileDest, true);
            
            assertTrue(new File(tempFileDest).exists());
            
        } catch(Exception e) {
            throw new RuntimeException("Unable to copy temp file during "
                    + "unit test!", e);
        }finally {
            
            if(new File(tempFileSource).exists()) {
                new File(tempFileSource).delete();
            }
            if(new File(tempFileDest).exists()) {
                new File(tempFileDest).delete();
            }
        }
        
        System.out.println("Copy operation testing complete.\n");
    }
    
    @Test
    public void testTemporaryFile() {
        
        System.out.println("Testing temporary file operations...");
        
        // Create a file with extension; no '.'
        final File tempFile1 = FileUtility.createTemporarySystemFile("txt");
        assertNotNull(tempFile1);
        assertTrue(tempFile1.exists());
        assertTrue(tempFile1.canRead());
        assertTrue(tempFile1.canWrite());
        System.out.println("Temp File Path: " + tempFile1.getAbsolutePath());
        
        // Delete and confirm
        boolean fileIsDeleted = tempFile1.delete();
        assertTrue(fileIsDeleted);
        assertFalse(tempFile1.exists());
        
        // Create a file with extension
        final File tempFile2 = FileUtility.createTemporarySystemFile(".txt");
        assertNotNull(tempFile2);
        assertTrue(tempFile2.exists());
        assertTrue(tempFile2.canRead());
        assertTrue(tempFile2.canWrite());
        System.out.println("Temp File Path: " + tempFile2.getAbsolutePath());
        
        // Delete and confirm
        fileIsDeleted = tempFile2.delete();
        assertTrue(fileIsDeleted);
        assertFalse(tempFile2.exists());
        
        // Create a file with no extension
        final File tempFile3 = FileUtility.createTemporarySystemFile();
        assertNotNull(tempFile3);
        assertTrue(tempFile3.exists());
        assertTrue(tempFile3.canRead());
        assertTrue(tempFile3.canWrite());
        System.out.println("Temp File Path: " + tempFile3.getAbsolutePath());
        
        // Delete and confirm
        fileIsDeleted = tempFile3.delete();
        assertTrue(fileIsDeleted);
        assertFalse(tempFile3.exists());
        
        System.out.println("Temporary file operation testing complete.\n");
    }
}

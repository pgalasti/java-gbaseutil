/*
 * (C) Copyright 2015 Paul Galasti
 *
 * https://bitbucket.org/pgalasti
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */
package com.galasti.gbaseutil.io;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author pgalasti
 */
public class TextCompressorUtilityTest {
    
    @BeforeClass
    public static void setUpClass() {
        
        System.out.println("====================================\n");
        System.out.println("Text Compressor Utility test starting...");
        System.out.println("====================================\n");
        
        
    }
    
    @AfterClass
    public static void tearDownClass() {
        
        
        
        System.out.println("====================================\n");
        System.out.println("Text Compressor Utility test complete!");
        System.out.println("====================================\n");
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    private static final String ORIGINAL_STRING = "THIS IS A TEST STRING "
            + "AAAAAABBBBBBBBCCCCCCCCDDDDDDEEEEEEEEEEEEEEEEEEEEEEEEEE222222222";
    
    @Test
    public void testUtility() {
    
        // Confirm the compressed string is at the least NOT larger than the 
        // original. (Rare cases it will be the same size)
        final String compressedString = TextCompressorUtility.compress(ORIGINAL_STRING);
        assertTrue(compressedString.length() <= ORIGINAL_STRING.length());
        
        // Confirm the decompressed string matches the original
        final String decompressedString = TextCompressorUtility.decompress(compressedString);
        assertTrue(decompressedString.equals(ORIGINAL_STRING));
        
    }
}
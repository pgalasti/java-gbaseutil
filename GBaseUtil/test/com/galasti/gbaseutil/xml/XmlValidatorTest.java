/*
 * (C) Copyright 2015 Paul Galasti
 *
 * https://bitbucket.org/pgalasti
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */
package com.galasti.gbaseutil.xml;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Paul Galasti
 */
public class XmlValidatorTest {
    
    @BeforeClass
    public static void setUpClass() {
        
        System.out.println("====================================\n");
        System.out.println("XML Validator test starting...");
        System.out.println("====================================\n");
        
    }
    
    @AfterClass
    public static void tearDownClass() {
        
        
        System.out.println("====================================\n");
        System.out.println("XML Validator test complete!");
        System.out.println("====================================\n");
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    final File simpleTestSchemaFile = new File("./testResources/simpleTestSchema.xsd");
    final File simpleTestDataFile = new File("./testResources/simpleTestData.xml");
    final File simpleInvalidDataFile = new File("./testResources/simpleInvalidTestData.xml");
    
    @Test
    public void validateSimpleSchema() {
    
        System.out.println("Performing simple schema validations...");
        
        // Load the validator with the simple schema
        XmlValidator validator = new XmlValidator(simpleTestSchemaFile);
        
        // Confirm it validates the simple test data
        assertTrue(validator.validate(simpleTestDataFile));
        
        // Confirm it fails the invalid test data.
        assertFalse(validator.validate(simpleInvalidDataFile));
        
        System.out.println("Simple schema validation test complete.");
    }
}
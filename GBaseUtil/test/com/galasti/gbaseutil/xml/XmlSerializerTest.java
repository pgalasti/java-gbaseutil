/*
 * (C) Copyright 2015 Paul Galasti
 *
 * https://bitbucket.org/pgalasti
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */
package com.galasti.gbaseutil.xml;

import static org.junit.Assert.assertTrue;

import java.io.File;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.galasti.gbaseutil.xml.XmlSerializer;

/**
 *
 * @author Paul Galasti
 */
public class XmlSerializerTest {
    
    public XmlSerializerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        
        System.out.println("====================================\n");
        System.out.println("XML Serializer test starting...");
        System.out.println("====================================\n");
        
    }
    
    @AfterClass
    public static void tearDownClass() {
        
        
        
        System.out.println("====================================\n");
        System.out.println("XML Serializer test complete!");
        System.out.println("====================================\n");
    }
    
    final File simpleTestDataFile = new File("./testResources/simpleTestData.xml");
    
    // Don't touch this string text, it's pulled from the XML test files
    // and is designed to confirm serialization is working appropriately.
    // ==================================================================
    final static String XML_DATA_NO_OPTIONS
            = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n" +
            "<note>\n" +
            "    <to>Tove</to>\n" +
            "    <from>Jani</from>\n" +
            "    <heading>Reminder</heading>\n" +
            "    <body>Don't forget me this weekend!</body>\n" +
            "</note>";
    
    final static String XML_DATA_NO_WHITESPACE
            = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?><note>\n" +
            "    <to>Tove</to>\n" +
            "    <from>Jani</from>\n" +
            "    <heading>Reminder</heading>\n" +
            "    <body>Don't forget me this weekend!</body>\n" +
            "</note>";
    
    final static String XML_DATA_NO_XML_HEADER
            = "<note>\n" +
            "    <to>Tove</to>\n" +
            "    <from>Jani</from>\n" +
            "    <heading>Reminder</heading>\n" +
            "    <body>Don't forget me this weekend!</body>\n" +
            "</note>";
    // ==================================================================\
    
    @Test
    public void serializeXmlFileTest() {
        System.out.println("Performing XML to String serialization test...");
        
        XmlSerializer xmlSerializer = new XmlSerializer();
        
        // Test it without options
        final String serializedXmlDataNoOptions = 
                xmlSerializer.serialize(simpleTestDataFile);
        assertTrue(serializedXmlDataNoOptions.contains(XML_DATA_NO_OPTIONS));
        
        // Test it without whitespace
        xmlSerializer.setOptions(XmlSerializer.REMOVE_WHITESPACE);
        final String serializedXmlDataNoWhitespace = 
                xmlSerializer.serialize(simpleTestDataFile);
        assertTrue(serializedXmlDataNoWhitespace.contains(XML_DATA_NO_WHITESPACE));
        
        // Test it without whitespace/Header
        xmlSerializer.setOptions(XmlSerializer.REMOVE_XML_DECL);
        final String serializedXmlDataNoWhitespaceOrHeader = 
                xmlSerializer.serialize(simpleTestDataFile);
        assertTrue(serializedXmlDataNoWhitespaceOrHeader.contains(XML_DATA_NO_XML_HEADER));
        
        System.out.println("XML to String serialization test complete!");
    }
}
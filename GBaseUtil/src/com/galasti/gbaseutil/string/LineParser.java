/*
 * (C) Copyright 2015 Paul Galasti
 *
 * https://bitbucket.org/pgalasti
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */
package com.galasti.gbaseutil.string;

import java.util.ArrayList;
import java.util.List;

/**
 * Simple line parser for parsing this.tokens using a {@link Character} 
 * delimiter.
 * 
 * @author Paul Galasti - pgalasti@gmail.com
 */
public class LineParser {

	private List<String> tokens;
	
	private String line;
	
	private char delimiter;
	
	/**
	 * Full constructor accepting a line and delimiter.
	 * <p>
	 * 
	 * @param line The line to be parsed.
	 * @param delimiter The delimiter to parse the string with.
	 */
	public LineParser(final String line, final char delimiter) {
		this.tokens = new ArrayList<>();
		setLineDelimiter(line, delimiter);
	}
	
	/**
	 * Blank constructor setting a blank line and a white space (dec 32) 0x0020 
	 * as the delimiter
	 * <p>
	 */
	public LineParser() {
		this("", ' ');
	}
	
	/**
	 * Loads a new {@link String} line and{@link Character} delimiter into the 
	 * parser.
	 * @param line New string line.
	 * @param delimiter New delimiter character.
	 */
	public void setLineDelimiter(final String line, final char delimiter) {
		setLine(line);
		setDelimiter(delimiter);
	}
	
	/**
	 * Loads a new {@link String} line into the parser.
	 * @param line New string line.
	 */
	public void setLine(final String line) {
		this.line = line;
	}
	
	/**
	 * Loads a new {@link Character} delimiter into the parser.
	 * @param delimiter New delimiter character.
	 */
	public void setDelimiter(final char delimiter) {
		this.delimiter = delimiter;
	}
	
	/**
	 * Parses the new line {@link String} with the new {@link Character} 
	 * delimiter character.
	 * @param line New string line.
	 * @param delimiter New delimiter character.
	 */
	public void parse(final String line, final char delimiter) {
		setLineDelimiter(line, delimiter);
		parse();
	}
	
	/**
	 * Parses the loaded line {@link String} with the loaded {@link Character} 
	 * delimiter character.
	 */
	public void parse()	{
		if( this.line.isEmpty() || this.delimiter == '\u0000')
			return;
		
		this.tokens.clear();
		
		int lineLength = this.line.length();

		StringBuffer sb = new StringBuffer();
		for(int i = 0; i < lineLength; i++)
		{
			char currentCharacter = this.line.charAt(i);
			if( currentCharacter == this.delimiter )
			{
				this.tokens.add(sb.toString());
				sb.delete(0, sb.length());
				continue;
			}
			
			sb.append(currentCharacter);
		}
		this.tokens.add(sb.toString());
	}
	
	/**
	 * @return A parsed token list.
	 */
	public ArrayList<String> getTokenList() {
		return new ArrayList<>(this.tokens);
	}
}

/*
 * (C) Copyright 2015 Paul Galasti
 *
 * https://bitbucket.org/pgalasti
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */
package com.galasti.gbaseutil.util;

import java.util.Calendar;

/**
 * Utility class used for wrapping date/time logic in a more simple class. 
 * 
 * @author Paul Galasti - pgalasti@gmail.com
 */
public class DateTime {

	/** Displays Date in <b>MM/DD/CCYY</b> string format from display methods.*/
	public static final int MMDDCCYY 	= 0x0001;
	/** Displays Date in <b>MM/DD/YY</b> string format from display methods.*/
	public static final int MMDDYY 		= 0x0002;
	/** Displays Date in <b>DD/MM/CCYY</b> string format from display methods.*/
	public static final int DDMMCCYY 	= 0x0004;
	/** Displays Date in <b>DD/MM/YY</b> string format from display methods.*/
	public static final int DDMMYY 		= 0x0008;
	
	/** Displays Time in <b>HH:MM:SS:MS</b> string format from display methods.
	*/
	public static final int HHMMSSMS 	= 0x0100;
	/** Displays Time in <b>HH:MM:SS</b> string format from display methods.*/
	public static final int HHMMSS 		= 0x0200;
	/** Displays Time in <b>HH:MM</b> string format from display methods.*/
	public static final int HHMM 		= 0x0400;
	
	private Calendar calendar = Calendar.getInstance();
	
	private long milliseconds;
	
	/**
	 * Static variable for milliseconds in a day.
	 */
	public final static long MILLISECONDS_IN_DAY 		= 86400000L;
	
	/**
	 * Static variable for milliseconds in an hour.
	 */
	public final static long MILLISECONDS_IN_HOUR		= 3600000L;
	
	/**
	 * Static variable for milliseconds in a minute.
	 */
	public final static long MILLISECONDS_IN_MINUTE		= 60000L;
	
	/**
	 * Static variable for milliseconds in a second.
	 */
	public final static long MILLISECONDS_IN_SECOND 	= 1000L;
	
	/**
	 * Returns the difference in days between the two dates. If the difference 
	 * of days is negative, <b>date1</b> is before <b>date2</b>. This method 
	 * excludes time of day.
	 * 
	 * @param date1 The first date.
	 * @param date2 The second date.
	 * @return The days between <b>date1</b> and <b>date2</b>
	 */
	public static int differenceBetweenDates(DateTime date1, DateTime date2) {
		
		// Remove time from calculation.
		date1.setTimeToStartOfDay();
		date2.setTimeToStartOfDay();
		
		int daysBetween = (int)((date2.getTimeInMilliseconds() - 
				date1.getTimeInMilliseconds()) / MILLISECONDS_IN_DAY);
		
		return daysBetween;
	}
	
	/**
	 * Default Constructor. The date/time will automatically be set to the GMT 
	 * time the Date object was initialized.
	 */
	public DateTime() {
		
		this.milliseconds = System.currentTimeMillis();
		this.calendar.set(Calendar.MILLISECOND, (int) this.milliseconds);
	}
	
	/**
	 * The date/time will be set according to the millisecond time given.
	 * @param milliseconds The date/time in milliseconds being initalized.
	 */
	public DateTime(long milliseconds) {
		setDateTime(milliseconds);
	}
	
	/**
	 * The date/time will be set according to the date/month/year given.
	 * The time of the date will be set as 0 representing the earliest time in
	 * the day.
	 * @param day The calendar date.
	 * @param month The calendar month.
	 * @param year The calendar year.
	 */
	public DateTime(int month, int day, int year) {
		
		this.calendar.set(Calendar.DAY_OF_MONTH, day);
		this.calendar.set(Calendar.MONTH, month-1);
		this.calendar.set(Calendar.YEAR, year);
		
		setTimeToStartOfDay();
		
		this.milliseconds = this.calendar.getTimeInMillis();
	}
	
	/**
	 * The date/time will be set according to the date/month/year:hour/minute/
	 * second/millisecond given.
	 * @param month
	 * @param day
	 * @param year
	 * @param hour
	 * @param minute
	 * @param second
	 * @param millisecond
	 */
	public DateTime(int month, int day, int year, int hour, int minute, int second, int millisecond) {
		
		this.calendar.set(Calendar.DAY_OF_MONTH, day);
		this.calendar.set(Calendar.MONTH, month-1);
		this.calendar.set(Calendar.YEAR, year);
		
		this.calendar.set(Calendar.HOUR_OF_DAY, hour);
		this.calendar.set(Calendar.MINUTE, minute);
		this.calendar.set(Calendar.SECOND, second);
		this.calendar.set(Calendar.MILLISECOND, millisecond);
		
		this.milliseconds = this.calendar.getTimeInMillis();
	}
	
	public void setDateTime(long milliseconds) {
		
		this.milliseconds = milliseconds;
		calendar.setTimeInMillis(this.milliseconds);
	}
	
	/**
	 * Returns the date/time in milliseconds from the epoch.
	 * @return The date/time in milliseconds from the epoch.
	 */
	public long getTimeInMilliseconds() {
		
		return this.milliseconds;
	}
	
	/**
	 * Sets the date/time to the current system time.
	 */
	public void setCurrent() {
		this.calendar.setTimeInMillis(System.currentTimeMillis());
	}
	
	/**
	 * Changes the date/time by the number of days passed. This value can be 
	 * positive or negative to traverse time. 
	 * @param days The number of days being added or subtracted to the date.
	 */
	public void changeDays(int days) {
		
		this.calendar.add(Calendar.DATE, days);
		this.milliseconds = calendar.getTimeInMillis();
	}
	
	/**
	 * Changes the date/time by the number of months passed. This value can be 
	 * positive or negative to traverse time.
	 * @param months The number of months being added or subtracted to the date.
	 */
	public void changeMonths(int months) {
		
		this.calendar.add(Calendar.MONTH, months);
		this.milliseconds = this.calendar.getTimeInMillis();
	}
	
	/**
	 * Changes the date/time by the number of years passed. This value can be 
	 * positive or negative to traverse time.
	 * @param years The number of years being added or subtracted to the date.
	 */
	public void changeYears(int years) {
		
		this.calendar.add(Calendar.YEAR, years);
		this.milliseconds = this.calendar.getTimeInMillis();
	}
	
	/**
	 * Returns the day of the month. 
	 * @return The day of the month.
	 */
	public int getDayOfMonth() {
		return this.calendar.get(Calendar.DATE);
	}
	
	/**
	 * Returns the month of the year.
	 * @return The month of the year.
	 */
	public int getMonth() {
		return this.calendar.get(Calendar.MONTH)+1;
	}
	
	/**
	 * Returns the year.
	 * @return The year.
	 */
	public int getYear() {
		return this.calendar.get(Calendar.YEAR);
	}
	
	/**
	 * Sets the date/time by setting the time to the beginning of the day.
	 * The time of the date will be set as 0 representing the earliest time in 
	 * the day.
	 */
	public void setTimeToStartOfDay() {
		
		this.calendar.set(Calendar.HOUR_OF_DAY, 0);
		this.calendar.set(Calendar.MINUTE, 0);
		this.calendar.set(Calendar.SECOND, 0);
		this.calendar.set(Calendar.MILLISECOND, 0);
		
		this.milliseconds = this.calendar.getTimeInMillis();
	}
	
	/**
	 * Displays the date in string format. Numerics are separated by the '/' 
	 * character.
	 * 
	 * @param displayOptions The format of the date.
	 * @return A string representation of just the date.
	 */
	public String dateToString(final int displayOptions) {
		StringBuilder sb = new StringBuilder();
		
		if ((MMDDCCYY & displayOptions) == MMDDCCYY)
		{
			sb.append(this.calendar.get(Calendar.MONTH)+1).append("/")
			.append(this.calendar.get(Calendar.DATE)).append("/")
			.append(this.calendar.get(Calendar.YEAR));
		}
		else if ((MMDDYY & displayOptions) == MMDDYY)
		{
			String lastTwoDigits = String.valueOf(this.calendar.get(Calendar.YEAR));
			lastTwoDigits = lastTwoDigits.substring(lastTwoDigits.length()-2);
			
			sb.append(this.calendar.get(Calendar.MONTH)+1).append("/")
			.append(this.calendar.get(Calendar.DATE)).append("/")
			.append(lastTwoDigits);
		}
		else if ((DDMMCCYY & displayOptions) == DDMMCCYY)
		{
			sb.append(this.calendar.get(Calendar.DATE)).append("/")
			.append(this.calendar.get(Calendar.MONTH)+1).append("/")
			.append(this.calendar.get(Calendar.YEAR));
		}
		else if ((DDMMCCYY & displayOptions) == DDMMCCYY)
		{
			String lastTwoDigits = String.valueOf(this.calendar.get(Calendar.YEAR));
			lastTwoDigits = lastTwoDigits.substring(lastTwoDigits.length()-2);
			
			sb.append(this.calendar.get(Calendar.MONTH)+1).append("/")
			.append(this.calendar.get(Calendar.DATE)).append("/")
			.append(lastTwoDigits);
		}
		
		return sb.toString();
	}
	
	/**
	 * Displays the time in string format. Numerics are separated by the ':' 
	 * character.
	 * 
	 * @param displayOptions The format of the time.
	 * @return A string representation of just the time.
	 */
	public String timeToString(int displayOptions) {
		
		StringBuilder sb = new StringBuilder();
		
		if ((HHMMSSMS & displayOptions) == HHMMSSMS)
		{
			sb.append(this.calendar.get(Calendar.HOUR_OF_DAY)).append(":")
			.append(this.calendar.get(Calendar.MINUTE)).append(":")
			.append(this.calendar.get(Calendar.SECOND)).append(":")
			.append(this.calendar.get(Calendar.MILLISECOND));
		}
		else if ((HHMMSS & displayOptions) == HHMMSS)
		{
			sb.append(this.calendar.get(Calendar.HOUR_OF_DAY)).append(":")
			.append(this.calendar.get(Calendar.MINUTE)).append(":")
			.append(this.calendar.get(Calendar.SECOND)).append(":");
		}
		else if ((HHMM & displayOptions) == HHMM)
		{
			sb.append(this.calendar.get(Calendar.HOUR_OF_DAY)).append(":")
			.append(this.calendar.get(Calendar.MINUTE)).append(":");
		}
		
		return sb.toString();
	}
	
	/**
	 * Displays the date/time in string format. 
	 * <p>
	 * Date numerics are separated by the '/'. Time numerics are seperated by 
	 * the ':' character.
	 * 
	 * @param displayOptions The format of the date/time.
	 * @return A string representation of just the date/time.
	 */
	public String dateTimeToString(int displayOptions) {
		
		StringBuilder sb = new StringBuilder();
		
		sb.append(dateToString(displayOptions)).append(" ")
		.append(timeToString(displayOptions));
		
		return sb.toString();
	}
}
/*
 * (C) Copyright 2015 Paul Galasti
 *
 * https://bitbucket.org/pgalasti
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */
package com.galasti.gbaseutil.io;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * Tool to compress/decompress <code>String</code> data using ISO-8859-1
 * character set.
 * @author Paul Galasti
 */
public class TextCompressorUtility {
    
    private static final String CHARACTER_SET = "ISO-8859-1";
    
    /**
     * Compresses text using GZIP compression format into ISO-8859-1 character 
     * set.
     * @param text Text to be compressed
     * @return Compressed text in ISO-8859-1 character set format
     */
    public static String compress(String text) {
        
        // Can't do anything with an empty string
        if(text == null || text.isEmpty()) {
            return "";
        }
        
        String compressedResult = "";
        GZIPOutputStream gzip = null;
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            
            // Write the stream to output
            gzip = new GZIPOutputStream(out);
            gzip.write(text.getBytes(CHARACTER_SET));
            
        } catch (IOException e) {
            throw new RuntimeException("Unable to compress xml text!", e);
        } finally {
            // This is ugly but required due to checked exceptions..
            try {
                if(gzip != null) {
                    gzip.close();
                }
                
                out.close();
            } catch (IOException e) {
                throw new RuntimeException("Unable to close gzip!", e);
            }
        }
        
        try {
        // Get the compressed output
        compressedResult = out.toString(CHARACTER_SET);
        } catch(UnsupportedEncodingException e) {
            throw new RuntimeException("Unable to identify encoding " 
                    + CHARACTER_SET, e);
        }
            
        return compressedResult;
    }
    
    /**
     * Decompress text using GZIP compression format from ISO-8859-1 character
     * set.
     * @param compressedText Compressed text in ISO-8859-1 character set
     * @return Decompressed text
     */
    public static String decompress(String compressedText) {
        
        // Can't do anything with an empty string
        if(compressedText == null || compressedText.isEmpty()) {
            return "";
        }
        
        String decompressedText = "";
        GZIPInputStream gzip = null;
        
        InputStream input = null; 
        try {
            // Read the bytes into GZIP
            byte[] decodedBytes = compressedText.getBytes(CHARACTER_SET);
            input = new ByteArrayInputStream(decodedBytes);
            
            gzip = new GZIPInputStream(input);
            
            // Setup a reader from GZIP
            InputStreamReader reader = new InputStreamReader(gzip, CHARACTER_SET);
            BufferedReader in = new BufferedReader(reader);
            
            // Read the text line for line
            String buffer;
            while ((buffer = in.readLine()) != null) {
                decompressedText += buffer;
            }
            
        } catch (IOException e) {
            throw new RuntimeException("Unable to decompress text!", e);
        } finally {
            
            // This is ugly but required due to checked exceptions..
            try {
                if(gzip != null) {
                    gzip.close();
                }
                if(input != null) {
                    input.close();
                }
            } catch (IOException e) {
                throw new RuntimeException("Unable to close gzip!", e);
            }
        }
        
        return decompressedText;
    }
}
/*
 * (C) Copyright 2015 Paul Galasti
 *
 * https://bitbucket.org/pgalasti
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */
package com.galasti.gbaseutil.io;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

/**
 * Tool encapsulating various File IO operations.
 * @author Paul Galasti
 */
public class FileUtility {
    
    /**
     * Copies a file from a full path source to a full path destination source.
     * @param source Full path of the source file to be copied.
     * @param dest Full path of the new destination file.
     * @param overwrite Specifies whether the destination file should be over
     * written. If <code>False</code> and a file exists at the destination path
     * a {@link RuntimeException} will be thrown.
     */
    public static void copy(final String source, final String dest, 
            boolean overwrite) {
        
        // Make sure the source file exists
        if(!new File(source).exists()) {
            throw new RuntimeException("Cannot find source file! File at "
                    + "path: " + dest + " already exists!");
        }
        
        if(!overwrite) {
            // If we're not overwriting and there's already a file at dest,
            // we need to throw an exception
            if(new File(dest).exists()) {
                throw new RuntimeException("Cannot copy! File at path: "
                        + dest + " already exists!");
            }
        }
        
        final Path sourcePath = Paths.get(source);
        final Path destinationPath = Paths.get(dest);
        
        try {
            
            if(overwrite) {
                Files.copy(sourcePath, destinationPath, REPLACE_EXISTING);
            } else {
                Files.copy(sourcePath, destinationPath);
            }
        } catch (IOException e) {
            throw new RuntimeException("Unable to copy file: " 
                    + source + " to: " + dest, e);
        }
    }
    
    /**
     * Copies a file specified in {@link File} objects.
     * @param source <code>File</code> object representing a source file
     * @param dest <code>File</code> object representing a destination file
     * @param overwrite Specifies whether the destination file should be over
     * written. If <code>False</code> and a file exists at the destination path
     * a {@link RuntimeException} will be thrown.
     */
    public static void copy(final File source, final File dest, 
            boolean overwrite) {

        copy(source.getAbsolutePath(), dest.getAbsolutePath(), overwrite);
    }
    
    /**
     * Returns the full path to the application user home directory. If the 
     * virtual machine is unable to acquire the user's home directory, a
     * {@link RuntimeException} will be thrown.
     * @return The full path to the user home directory.
     */
    public static String getUserHomeDirectory() {
        final String userHomeDirectory = System.getProperty("user.home");
        
        if(userHomeDirectory == null) {
            throw new RuntimeException("Unable to acquire a user home "
                    + "directory!");
        }
        
        return userHomeDirectory;
    }
    
    /**
     * Creates and returns a JVM temporary system file with a given extension.
     * If the file is not deleted earlier in usage, the file will be deleted in
     * JVM termination. 
     * @param extension The extension of the temporary file. The extension '.' 
     * character will be added if not specified as the first character.
     * @return A temporary system file
     */
    public static File createTemporarySystemFile(final String extension) {
        
        final String prefix = UUID.randomUUID().toString();
        final StringBuilder extBuilder = new StringBuilder("");
        
        if(!extension.isEmpty()) {
            
            if(extension.charAt(0) != '.') {
                extBuilder.append(".");
            }
            
            extBuilder.append(extension);
            
        } 
        
        final File tempFile;
        try {
            tempFile = File.createTempFile(prefix, extBuilder.toString());
        } catch (IOException e) {
            throw new RuntimeException("Unable to create a temporary system "
                    + "file!", e);
        }
        
        tempFile.deleteOnExit();
        return tempFile;
    }
    
    /**
     * Creates and returns a JVM temporary system file with no extension.
     * If the file is not deleted earlier in usage, the file will be deleted in
     * JVM termination. 
     * @return A temporary system file
     */
    public static File createTemporarySystemFile() {
        return createTemporarySystemFile("");
    }
    
}

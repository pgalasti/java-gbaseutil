/*
 * (C) Copyright 2015 Paul Galasti
 *
 * https://bitbucket.org/pgalasti
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */
package com.galasti.gbaseutil.xml;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 * Performs read and parse operations on an XML file to serialize data as 
 * <code>String</code> data.
 * <br/>
 * @author Paul Galasti
 */
public class XmlSerializer {
    
    protected static long BLANK_OPTIONS       = 0x0000;
    
    protected long options;
    
    /**
     * Option to specify no whitespace between XML elements and attributes when
     * not needed.
     */
    public static long REMOVE_WHITESPACE    = 0x0002;
    
    /**
     * Option to specify no top level XML element in the serialized output.
     */
    public static long REMOVE_XML_DECL       = 0x0004;
    
    /**
     * Creates a new <code>XmlSerializer</code> object with no options for 
     * serialization.
     */
    public XmlSerializer() {
        this.setOptions(BLANK_OPTIONS);
    }
    
    /**
     * Creates a a new <code>XmlSerializer</code> object with specified options 
     * for serialization.
     * @param serializeOptions Bit options for serialization
     */
    public XmlSerializer(long serializeOptions) {
        this.setOptions(serializeOptions);
    }
    
    /**
     * Sets the serialization bit options.
     * @param serializeOptions Bit options being set for serialization
     */
    final public void setOptions(long serializeOptions) {
        this.options |= serializeOptions;
    }
    
    /**
     * Un-sets the serialization bit options.
     * @param serializeOptions Bit options being unset for serialization
     */
    final public void unsetOptions(long serializeOptions) {
        this.options &= serializeOptions;
    }
    
    /**
     * Serializes an XML file into <code>String</code> format.
     * <br/>
     * The serialized <code>String</code> format is based on the object 
     * serialization options.
     * @param xmlFile An XML file.
     * @return A <code>String</code> serialization of an XML file.
     */
    public String serialize(File xmlFile) {
        
        StreamResult result = null;
        try {
            // Create a builder to parse the XML file
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document parsedDocument = builder.parse(xmlFile);
            
            // Setup the transformer properties based on bit options
            final String encodingType = parsedDocument.getInputEncoding();

            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.METHOD, "xml");
            transformer.setOutputProperty(OutputKeys.ENCODING, encodingType);
            
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, 
                    (this.options & REMOVE_XML_DECL) == REMOVE_XML_DECL ? "yes" : "no");
            transformer.setOutputProperty(OutputKeys.INDENT, 
                    (this.options & REMOVE_WHITESPACE) == REMOVE_WHITESPACE ? "no" : "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", 
                    (this.options & REMOVE_WHITESPACE) == REMOVE_WHITESPACE ? "0" : "4");
            
            // Create a string writer to use for transformer result
            StringWriter stringWriter = new StringWriter();
            transformer.transform(new DOMSource(parsedDocument),
                    new StreamResult(stringWriter));
            
            // Create the cleaned document
            Document cleanedDocument = builder.parse(
                    new ByteArrayInputStream(stringWriter.toString().getBytes()));
            
            // Make the transformation on the result using the cleaned document
            DOMSource source = new DOMSource(cleanedDocument);
            result = new StreamResult(new StringWriter());
            transformer.transform(source, result);
            
        } catch (ParserConfigurationException | SAXException 
                | IOException | TransformerException e) {
            throw new RuntimeException("Unable to serialize XML file!", e);
        }
        
        return result.getWriter().toString();
        
    }
}
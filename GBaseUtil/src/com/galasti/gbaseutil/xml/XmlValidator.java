/*
 * (C) Copyright 2015 Paul Galasti
 *
 * https://bitbucket.org/pgalasti
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */
package com.galasti.gbaseutil.xml;

import java.io.File;
import java.io.IOException;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.xml.sax.SAXException;

/**
 * A utility for validating XML files with an XSD schema.
 * @author Paul Galasti
 */
public class XmlValidator {
    
    
    protected Validator validator;
    
    /**
     * Constructs a new <code>XmlValidator</code> object with an XSD schema.
     * @param xsdSchema The XSD schema to load the validator with.
     */
    public XmlValidator(File xsdSchema) {
        
                
        // Validate schema ends with xsd
        final String schemaName = xsdSchema.getName().toLowerCase();
        if(!schemaName.endsWith(".xsd")) {
            
            throw new RuntimeException("Schema file extension isn't xsd!" + 
                    xsdSchema.getName());
        }
        
        SchemaFactory schemaFactory = SchemaFactory
                .newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        
        // Load the schema
        Schema schema = null;
        try {
            schema = schemaFactory.newSchema(xsdSchema);
        } catch (SAXException e) {
            throw new RuntimeException("Unable to load schema factory: ", e);
        }
        
        // Create our validator
        this.validator = schema.newValidator();
    }
    
    /**
     * Validates whether an XML file meets the validation requirements of an 
     * XSD schema.
     * @param xmlFile An XML file
     * @return <b>True</b> if the XML file meets schema requirements. 
     * <b>False</b> if the XML file doesn't meet schema requirements.
     */
    public boolean validate(File xmlFile) {
        
        
        // Get the source interface from the file
        Source xmlSourceFile = new StreamSource(xmlFile);
        
        try {
            // Perform the validation
            this.validator.validate(xmlSourceFile);
        } catch (SAXException e) {
            // If it's a general validation error, we will just return false.
            return false;
        } catch (IOException e) {
            // Any issue with IO we want to throw as an exception.
            throw new RuntimeException("Unable to validate : ", e);
        }
        
        return true;
    }
}
/*
 * (C) Copyright 2015 Paul Galasti
 *
 * https://bitbucket.org/pgalasti
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */
package com.galasti.gbaseutil.db;

/**
 * Interface specifying object has readable database behavior.
 * 
 * @author Paul Galasti - pgalasti@gmail.com
 */
public interface IReadable {

	/**
	 * Selects data based on specified hard written SQL query.
	 * @param db The Database to query.
	 * @param sqlQuery The string SQL query.
	 */
	void select(Database db, String sqlQuery);
	
	/**
	 * Selects data based on loaded record fields.
	 * @param db The Database to query.
	 * @param keys The number of keys in column order to select on. The object must extend using the AQueryRecord class and 
	 * specify the fields @Override the BindData() method.
	 * @throws DBException
	 */
	void select(Database db, int keys);
}

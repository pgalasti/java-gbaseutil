/*
 * (C) Copyright 2015 Paul Galasti
 *
 * https://bitbucket.org/pgalasti
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */
package com.galasti.gbaseutil.db;

/**
 * An interface for describing specific table information.
 * 
 * @author Paul Galasti - pgalasti@gmail.com
 */
public interface IRecordAttributes {

	/**
	 * Returns the database table name.
	 * @return The database table name.
	 */
	String getTableName();
	
	/**
	 * Returns the columns of the table.
	 * @return The columns of the table.
	 */
	int getTableColumns();
	
	/**
	 * The SQL creation statement for the table.
	 * @return The SQL creation statement for the table.
	 */
	String getSQLCreateStatement();
	
	/**
	 * The SQL alteration statement for the table between versions. Each version 
	 * jump should be defined here and how it is handled.
	 * @param oldVersion The old version number the current database table is 
	 * coming from.
	 * @param newVersion The new version number the database table supports.
	 * @return The SQL alteration statement for the table between versions.
	 */
	String getSQLAlterSchema(int oldVersion, int newVersion);
}

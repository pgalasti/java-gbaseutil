/*
 * (C) Copyright 2015 Paul Galasti
 *
 * https://bitbucket.org/pgalasti
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */
package com.galasti.gbaseutil.db;

/**
 * Interface specifying object has writable database behavior. 
 * 
 * @author Paul Galasti - pgalasti@gmail.com
 */
public interface IWritable {

	/**
	 * Update the in memory data fields to the database. The object must extend 
	 * using the {@link QueryRecord} class and specify the fields 
	 * <code>@Override</code> the BindData() method.
	 * @param db The Database to perform the update on.
	 * @return The number of rows updated.
	 */
	int update(Database db);
	
	/**
	 * Insert the in memory data fields to the database. The object must extend 
	 * using the {@link QueryRecord} class and specify the fields 
	 * <code>@Override</code> the BindData() method.
	 * @param db The Database to perform the insert on.
	 * @return The row ID of the inserted row.
	 */
	long insert(Database db);
	
	/**
	 * Delete the in memory data fields to the database. The object must extend 
	 * using the {@link QueryRecord} class and specify the fields 
	 * <code>@Override</code> the BindData() method.
	 * @param db The Database to perform the deletion on.
	 * @return The number of rows deleted.
	 */
	int delete(Database db);
}

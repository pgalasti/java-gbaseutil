/*
 * (C) Copyright 2015 Paul Galasti
 *
 * https://bitbucket.org/pgalasti
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */
package com.galasti.gbaseutil.db;


/**
 * Abstract database with basic functionality to represent a database.
 * <p>
 * 
 * The inherited classes will contain specific {@link ICursor} implementations
 * for a specific database. Inherited classes will also contain specific logic
 * for database connection/metadata per database implementation. 
 * 
 * @author Paul Galasti pgalasti@gmail.com
 */
public abstract class Database {

	/**
	 * Inherited implementation will return an implemented cursor for 
	 * traversing query data.
	 * @return The cursor type for the database.
	 */
	protected abstract ICursor getDatabaseCursor();
	
	/**
	 * Inherited implementation will close connection to the database.
	 */
	public abstract void close();
	
	/**
	 * Inherited implementation will open connection with the database.
	 */
	public abstract void connect();
	
	/**
	 * Inherited implementation will return the DBMS name.
	 * @return The DBMS name.
	 */
	public abstract String getDBMSName();
	
	/**
	 * Inherited implementation will return  the DBMS Version.
	 * @return The DBMS Version.
	 */
	public abstract String getDBMSVersion();
}

/*
 * (C) Copyright 2015 Paul Galasti
 *
 * https://bitbucket.org/pgalasti
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */
package com.galasti.gbaseutil.db;


/**
 * Basic database record layer between application logic and database. Each 
 * child of <code>QueryRecord</code> should represent a specific table of the 
 * database.
 * <p>
 * 
 * <code>QueryRecord</code> contain methods traversing the cursor state of a 
 * table queried along the ability to update corresponding database records 
 * with data loaded in secondary fields.
 * <p>
 *
 *  <code>QueryRecord</code> follow the {@link IWritable} 
 *  and {@link IReadable} implementing creation for specific SQL strings and 
 *  executing SQL commands within the <code>QueryRecord</code>.
 *  <p>
 *    
 * @author pgalasti@gmail.com
 */
public abstract class QueryRecord implements IWritable, IReadable {

	protected ICursor cursor;
	
	/**
	 * Creates a <code>QueryRecord</code> object assigned to a particular 
	 * database.
	 * @param database The database the <code>QueryRecord</code> is assigned to.
	 */
	public QueryRecord(Database database) {
		this.cursor = database.getDatabaseCursor();
	}
	
	/**
	 * First result of query performed.
	 */
	public void getFirst()
	{
		if( this.cursor.getCount() == 0 ) {
			return;
		}
		
		this.cursor.toFirst();
		
		if(!this.cursor.isGoodState()) {
			throw new RuntimeException("Cursor is not in a state to get first"
					+" record");
		}
		
		bindData();
	}
	
	/**
	 * Last result of query performed.
	 */
	public void getLast()
	{
		if( this.cursor.getCount() == 0 ) {
			return;
		}
		
		this.cursor.toLast();
		
		if(!this.cursor.isGoodState()) {
			throw new RuntimeException("Cursor is not in a state to get last"
					+" record");
		}
		
		bindData();
	}
	
	/**
	 * Moves cursor to next row of query.
	 * @return 	<b>True</b> if cursor moved to next row.
	 * 			</br><b>False</b> if there are no more rows.
	 */
	public boolean readNext()
	{
		if(!this.cursor.isGoodState()) {
			throw new RuntimeException("Cursor is not in a state to read next");
		}
		
		if( !this.cursor.next() ) {
			return false;
		}
		
		bindData();
		
		return true;
	}
	
	/**
	 * Binds current cursor result to object instance record data. Child 
	 * classes should override with how their records bind with database 
	 * fields.
	 */
	protected abstract void bindData();
	
	/**
	 * Returns an implemented {@link IRecordAttributes} containing all 
	 * important database table information.
	 * <p>
	 * 
	 * @return The extended <code>QueryRecord</code> specific 
	 * {@link IRecordAttributes}.
	 */
	public abstract IRecordAttributes getRecordAttributes();
	
	/**
	 * Returns a string detailing the current record field data. This should 
	 * only be for debug information.
	 * <p>
	 * 
	 * String should be specific to table layout, but should follow convention 
	 * of simple field to value detail.
	 * <p>
	 * 
	 * Use convention of: </br>
	 * <b>[field1]:[type]:[value]</b>[NEWLINE]</br>
	 * <b>[field2]:[type]:[value]</b>[NEWLINE]</br>
	 * <b>etc...</b></br>
	 * <p>
	 * 
	 * @return String detailing current record field data.
	 */
	public abstract String getDebugDetail();
}

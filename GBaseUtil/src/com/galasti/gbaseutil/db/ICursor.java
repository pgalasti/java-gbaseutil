/*
 * (C) Copyright 2015 Paul Galasti
 *
 * https://bitbucket.org/pgalasti
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */
package com.galasti.gbaseutil.db;

/**
 * Interface representing a database cursor behavior.
 * 
 * @author Paul Galasti - pgalasti@gmail.com
 */
public interface ICursor {

	/**
	 * Inherited interface will return the count of the result set. 
	 * @return Count of the result set
	 */
	int getCount();
	
	/**
	 * Inherited interface will move cursor location to the first result in
	 * the result set.
	 */
	void toFirst();
	
	/**
	 * Inherited interface will move cursor location to the last result in the
	 * result set.
	 */
	void toLast();
	
	/**
	 * Inherited interface will move cursor to the <u>next</u> location in the 
	 * result set if it can. 
	 * <p>
	 * The method will return <b>True</b> if it is able to do this with the
	 * result set.
	 * <p>
	 * The method will return <b>False</b> if unable to do so.
	 * @return <b>True</b> if able to move to next result set. <b>False</b> if 
	 * not able to move to next result set.
	 */
	boolean next();
	
	/**
	 * Inherited interface will move cursor to the <u>previous</u> location in 
	 * the result set if it can. 
	 * <p>
	 * The method will return <b>True</b> if it is able to do this with the
	 * result set.
	 * <p>
	 * The method will return <b>False</b> if unable to do so.
	 * @return <b>True</b> if able to move to previous result set. <b>False</b>
	 * if not able to move to previous result set.
	 */
	boolean previous();
	
	/**
	 * Inherited interface will return a <code>boolean</code> whether the cursor
	 * is in a good state.
	 * @return <b>True</b> if <code>ICursor</code> is in a valid state. 
	 * <b>False</b> if not .
	 */
	boolean isGoodState();
	
	/**
	 * Returns the <code>String</code> value of the current cursor position in
	 * the result set.
	 * @return The <code>String</code> at the current cursor position.
	 */
	String getString();
	
	/**
	 * Returns the <code>int</code> value of the current cursor position in
	 * the result set.
	 * @return The <code>int</code> at the current cursor position.
	 */
	Integer getInt();
	
	/**
	 * Returns the <code>long</code> value of the current cursor position in
	 * the result set.
	 * @return The <code>long</code> at the current cursor position.
	 */
	Long getLong();
	
	/**
	 * Returns the <code>boolean</code> value of the current cursor position in
	 * the result set.
	 * @return The <code>boolean</code> at the current cursor position.
	 */
	Boolean getBoolean();
	
}
